// 30 * 30
class SeatNode {
  constructor(el, key) {
    this.key = key
    if (typeof el === 'string') {
      this.el = document.querySelector(el)
    } else if (typeof el === 'object' && this.el) {
      this.el = el
    } else {
      this.el = this.createNode()
    }
  }

  createNode () {
    let node = document.createElement('div')
    node.id = this.key
    node.className = "seat"
    node.dataset.choosed = '0'
    return node 
  }
}

const seats = new Array(30 * 30).fill(Object.assign({}, {
  choose: false,
  node: null
}))

const docfrag = document.createDocumentFragment()
seats.forEach((s, i) => {
  s.node = new SeatNode(null, "seat-" + i)
  docfrag.appendChild(s.node.el)
})

var stats = new Stats();
stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
stats.domElement.style.cssText = 'position:fixed;top:0px;left:0px;';
document.body.appendChild(stats.dom);

var statsDom = new Stats();
statsDom.showPanel(3); // 0: fps, 1: ms, 2: mb, 3+: custom
// statsDom.Panel("DOM Nodes", "#0ff", "#002")
var xPanel = stats.addPanel(new Stats.Panel('DOM Nodes', '#ff8', '#221'))
statsDom.domElement.style.cssText = 'position:fixed;top:0px;left:80px;z-index:20';
document.body.appendChild(statsDom.dom);


window.addEventListener('load', () => {
  const container = document.querySelector(".main")
  stats.begin()
  container.appendChild(docfrag)
  stats.end()
  container.addEventListener('click', (event) => {
    if(event.target.className.indexOf('seat') <= -1) {
      return
    }
    const ele = event.target
    if ('0' === ele.dataset.choosed) {
      stats.begin()
      ele.dataset.choosed = '1'
      addClass(ele, 'choosed')
      ele.className += '  '
      stats.end()
      return
    }
    ele.dataset.choosed = '0'
    removeClass(ele, 'choosed')
  })
})
setInterval(function () {
  stats.update()
  statsDom.update()

}, 1000/60);

setInterval(function () {
  xPanel.update(document.querySelector(".main").children.length, 2000);
  statsDom.update()
}, 1000);
function addClass (ele, newClass) {
  if (newClass === undefined || newClass.trim() === '') {
    return
  }
  newClass = newClass.trim()
  let classes = ele.className.split(' ')
  if (classes.indexOf(newClass) > -1) {
    return 
  }
  classes.push(newClass)
  ele.className = classes.join(' ')
}

function removeClass(ele, className) {
  if (className === undefined || className.trim() === '') {
    return
  }
  className = className.trim()
  let classes = ele.className.split(' ')
  let classIndex = classes.indexOf(className)
  if (classIndex > -1) {
    classes.splice(classIndex, 1)
    ele.className = classes.join(' ')
  }
}