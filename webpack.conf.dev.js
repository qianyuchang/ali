module.exports = {
  mode: 'development',
  devServer: {
    contentBase: './',
    compress: true,
    port: 9000
  },
  devtool: '#cheap-module-eval-source-map'
}